class UserMailer < ActionMailer::Base
  default :from => "thenelse@rocketmaill.com"
  def registration_confirmation(user)
    mail(:to => user.email, :subject => "Registered")
  end
end

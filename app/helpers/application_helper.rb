module ApplicationHelper
  def sign_up_link
    "#{link_to "Signup", sign_up_path}"
  end

  def home_link
    "#{link_to "Home", root_url}"
  end

  def side_box
    str = ""
    str += "<ul>"
    str += "<li>#{link_to "Edit Profile", edit_user_path(current_user.id) }</li>"
    str += "<li>#{link_to "Sell a Product", new_product_path}</li>"
    str += "<li>#{link_to "Share an Article", new_article_path}</li>"
    str += "<li>#{link_to "Logout", log_out_path}</li>"
    str += "</ul>"
  end

  def parent_category(id)
    if id.nil?
      return "-"
    else
      category = Category.find_by_id(id)
      if category.nil?
        return "-"
      else
        return category.name
      end
    end
  end

  def gender(code)
    return code ? "Male" : "Female"
  end

  def category_name(id)
    return Category.find_by_id(id).name
  end

  def commentator(id)
    user = User.find_by_id(id)
    if current_user
      if user.id == current_user.id
        return "You"
      else
        return user.name.blank? ? "#{user.email}" : "#{user.name}"
      end
    else
      return user.name.blank? ? "#{user.email}" : "#{user.name}"
    end
  end

  def parent_category_name(id)
    category = Category.find_by_id(id)
    if category.parent_id
      return Category.find_by_id(category.parent_id).name
    else
      ""
    end
  end
end

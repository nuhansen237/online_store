class Comment < ActiveRecord::Base
  attr_accessible :comment, :article_id, :user_id
  belongs_to :user
  has_many :comments
end

class User < ActiveRecord::Base
  attr_accessible :address, :birthday, :blog, :email, :facebook, :gender, :is_admin, :name, :password_hash, :password_salt, :phone_number, :password, :password_confirmation
  
  attr_accessor :password

  before_save :encrypt_password
  validates :password, :presence => {:on => :create},
            :confirmation => true
  validates :email, :presence => true, :uniqueness => true

  has_many :articles, :dependent => :destroy
  has_many :products, :dependent => :destroy
  
  def encrypt_password
    if password.present?
      self.password_salt = BCrypt::Engine.generate_salt
      self.password_hash = BCrypt::Engine.hash_secret(password, password_salt)
    end
  end

  def self.authenticate(email, password)
    user = find_by_email(email)
    if user && user.password_hash == BCrypt::Engine.hash_secret(password, user.password_salt)
      user
    else
      nil
    end
  end
end

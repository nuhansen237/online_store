class Article < ActiveRecord::Base
  attr_accessible :body, :title, :user_id

  belongs_to :user
  has_many :comments, :dependent => :destroy

  validates :body, :presence => true
  validates :title, :presence => true
end

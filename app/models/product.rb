class Product < ActiveRecord::Base
  attr_accessible :category_id, :description, :name, :price, :user_id, :weight

  validates :name, :presence => true

  belongs_to :user
end

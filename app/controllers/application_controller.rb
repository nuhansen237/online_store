class ApplicationController < ActionController::Base
  protect_from_forgery
  helper_method :current_user

  protected
    def require_login
      if current_user.nil?
        flash[:error] = "You are not permitted, please login first"
        redirect_to root_url
      else
        return current_user
      end
    end

  private
    def current_user
      @current_user ||= User.find(session[:user_id]) if session[:user_id]
    end

    def is_admin
      current_user.email == "admin@admin.com"
    end

end

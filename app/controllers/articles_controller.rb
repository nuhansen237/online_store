class ArticlesController < ApplicationController
  before_filter :require_login, :only => [:new, :create, :edit, :update, :destroy]
  before_filter :find_article, :only => [:show, :edit, :update, :destroy]
  
  def index
    @articles = Article.all
  end

  def new
    @article = Article.new
  end

  def create
    @article = current_user.articles.new(params[:article])
    if @article.save
      flash[:notice] = "Your article has been created"
      redirect_to articles_path
    else
      flash[:error] = "Your article has failed to create"
      render :new
    end
  end

  def show
    @comment = Comment.new
    @comments = @article.comments
  end

  def edit
    unless session[:user_id].nil?
      unless current_user.is_admin
        unless current_user.id == @article.user_id
          flash[:error] = "You are not permitted to access the page"
          redirect_to articles_path
        end
      end
    end
  end

  def update
    if @article.update_attributes(params[:article])
      flash[:notice] = "Your article has been updated"
      redirect_to articles_path
    else
      flash[:error] = "Your article is failed to update"
      render :edit
    end
  end

  def destroy
    if @article.destroy
      flash[:notice] = "Your article has been destroyed"
      redirect_to articles_path
    else
      flash[:error] = "Your article is failed to destroy"
      redirect_to articles_path
    end
  end

  private
    def find_article
      @article = Article.find_by_id(params[:id])
      if @article.nil?
        flash[:error] = "Article was not found"
        redirect_to articles_path
      end
    end
  
end

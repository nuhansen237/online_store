class CategoriesController < ApplicationController
  before_filter :require_login, :only => [:show]
  before_filter :find_category, :only => [:show]

  def show
    @category = Category.find_by_id(params[:id])
    unless @category.parent_id.nil?
      @products = Product.where("category_id = ?", params[:id])
    else
      categories = Category.where("parent_id = ?", @category.id)
      child_categories = []
      categories.each do |category|
        child_categories << category.id
      end
      @products = Product.where("category_id = ?", params[:id])
      child_categories.each do |child_category|
        child_products = Product.where("category_id = ?", child_category)
        @products = @products + child_products
      end
    end
  end

  private
    def find_category
      @category = Category.find_by_id(params[:id])
      if @category.nil?
        flash[:notice] = "Category was not found"
        redirect_to root_url
      end
    end
end

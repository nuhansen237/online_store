class HomeController < ApplicationController
  def index
    @products = Product.order('id DESC').limit(6).all
    @articles = Article.order('id DESC').limit(3).all
  end
end

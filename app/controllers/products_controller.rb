class ProductsController < ApplicationController
  before_filter :require_login, :only => [:new, :create, :update, :edit, :destroy]
  before_filter :find_product, :only => [:show, :edit, :update, :destroy]

  def index
    @products = Product.all
  end

  def new
    @product = Product.new
    @categories = Category.all.map{|x| [x.name, x.id]}
  end

  def create
    @product = current_user.products.new(params[:product])
    @categories = Category.all.map{|x| [x.name, x.id]}
    if @product.save
      flash[:notice] = "Your product has been created"
      redirect_to products_path
    else
      flash[:error] = "Your product failed to create"
      render :new
    end
  end

  def show
  end

  def edit
    @categories = Category.all.map{|x| [x.name, x.id]}
    unless @product.nil?
      unless current_user.is_admin
        unless current_user.id == @product.user_id
          flash[:error] = "The product is not yours"
          redirect_to products_path
        end
      end
    end
  end

  def update
    @categories = Category.all.map{|x| [x.name, x.id]}
    if @product.update_attributes(params[:product])
      flash[:notice] = "Your product has been updated"
      redirect_to products_path
    else
      flash[:error] = "Your product failed to update"
      render :edit
    end
  end

  def destroy
    if @product.destroy
      flash[:notice] = "Your product has been destroyed"
      redirect_to products_path
    else
      flash[:error] = "Your product is failed to destroy"
      redirect_to products_path
    end
  end

  private
    def find_product
      @product = Product.find_by_id(params[:id])
      if @product.nil?
        flash[:notice] = "Product was not found"
        redirect_to root_url
      end
    end
end

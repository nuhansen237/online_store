class Admin::CategoriesController < Admin::ApplicationController
  before_filter :require_admin
  before_filter :find_category, :only => [:show, :edit, :update, :destroy]
  before_filter :parent_category, :only => [:new, :create, :edit]
  
  def index
    @categories = Category.all
  end

  def new
    @category = Category.new
  end

  def create
    @category = Category.new(params[:category])
    if @category.save
      flash.now[:notice] = "Category has been created"
      redirect_to admin_categories_path
    else
      flash.now[:error] = "Category failed to create"
      render :new
    end
  end

  def show
  end

  def edit
  end

  def update
    if @category.update_attributes(params[:category])
      flash[:notice] = "Category has been updated"
      redirect_to admin_categories_path
    else
      flash[:error] = "Category failed to update"
      render :edit
    end
  end

  def destroy
    if @category.destroy
      flash[:notice] = "Category has been deleted"
      redirect_to admin_categories_path
    else
      flash[:error] = "Category has failed to delete"
      redirect_to admin_categories_path
    end
  end

  private
  def find_category
    @category = Category.find_by_id(params[:id])
    if @category.nil?
      flash[:error] = "Category was not found"
      redirect_to admin_categories_path
    end
  end

  def parent_category
    @parent_category = Category.where(["parent_id IS NULL"]).map{|x| [x.name, x.id]}
  end
  
end

class Admin::ApplicationController < ApplicationController
  protect_from_forgery
  helper_method :current_user

  protected
  def require_admin
    if !current_user || !current_user.is_admin?
      flash[:error] = "You are not permitted, please login as admin first to see this page"
      redirect_to root_url
    else
      return current_user
    end
  end
end

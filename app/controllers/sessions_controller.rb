class SessionsController < ApplicationController
  def new
    redirect_to root_url
  end

  def create
    user = User.authenticate(params[:email], params[:password])
    if user
      session[:user_id] = user.id
      if user.email.eql?("admin@admin.com")
        redirect_to admin_categories_path, :notice => "Admin logged in!"
      else
        redirect_to user_path(user.id), :notice => "Logged in!"
      end
    else
      flash.now.alert = "Invalid email or password"
      render "new"
    end
  end

  def destroy
    session[:user_id] = nil
    redirect_to root_url, :notice => "Logged out!"
  end
end

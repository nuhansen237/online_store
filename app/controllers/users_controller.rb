class UsersController < ApplicationController
  before_filter :require_login, :only => [:edit, :update]
  before_filter :find_user, :only => [:show, :edit, :update]

  def new
    if current_user
      flash[:notice] = "You are currently signed up to this site"
      redirect_to root_url
    else
      @user = User.new
    end
  end

  def create
    @user = User.new(params[:user])
    if verify_recaptcha
      if @user.save
        UserMailer.registration_confirmation(@user).deliver
        session[:user_id] = @user.id
        redirect_to root_url, :notice => "Signed up!"
      else
        render "new"
      end
    else
      flash[:error] = "There was an error with the recaptcha code below.
                       Please re-enter the code and click submit."
      render "new"
    end
  end

  def show
  end

  def edit
    @genders = {
      'Male' => 1,
      'Female' => 0
    }
    unless current_user.id == @user.id
      redirect_to root_url, :notice => "You are not permitted to access the page"
    end
  end

  def update
    if @user.update_attributes(params[:user])
      flash[:notice] = "Your profile has been updated"
      redirect_to user_path(current_user.id)
    else
      flash[:error] = "Your profile is failed to save"
      render :edit
    end
  end

  private
    def find_user
      @user = User.find_by_id(params[:id])
      if @user.nil?
        flash[:notice] = "User not found"
        redirect_to root_url
      end
    end
end

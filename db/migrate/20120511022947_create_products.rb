class CreateProducts < ActiveRecord::Migration
  def change
    create_table :products do |t|
      t.string :name
      t.text :description
      t.decimal :price, :precision => 6, :scale => 1
      t.decimal :weight, :precision => 3, :scale => 1
      t.integer :user_id
      t.integer :category_id

      t.timestamps
    end
  end
end
